extern crate rand;
use crate::rand::Rng;
use std::path::Path;
use std::process::Command;
use std::env;


fn main() {
    let path = get_wallpaper_path();
    println!("Using a random wallpaper in {}", path);
    let file: std::string::String = get_random_wallpaper(Path::new(&path));
    change_wallpaper(file);
}

fn get_wallpaper_path() -> std::string::String {
    match env::var("WALLPAPER_FOLDER"){
        Ok(v)=>v,
        Err(_)=>panic!("WALLPAPER_FOLDER evironnement variable is not defined")
    }
}

fn get_random_wallpaper(path: &std::path::Path) -> std::string::String {
    let paths = std::fs::read_dir(path).unwrap();
    let mut wallpapers = Vec::new();
    for path in paths {
        let file: std::path::PathBuf = path.unwrap().path();
        match file.to_str() {
            Some(x) => wallpapers.push(x.to_string()),
            None => println!("no string representation"),
        }
    }
    let mut rng = rand::thread_rng();
    let num = rng.gen_range(0, wallpapers.len());
    println!("{}", wallpapers[num]);
    wallpapers[num].to_string()
}

fn change_wallpaper(file: std::string::String) {
    Command::new("feh")
        .arg("--bg-scale")
        .arg(file)
        .output()
        .expect("failed to execute process");
}
